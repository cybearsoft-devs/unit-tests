pytest==7.1.2
pytest-cov==3.0.0
pytest-cases==3.6.13
coverage==6.4.1
boto3==1.23.8
freezegun==0.3.4
