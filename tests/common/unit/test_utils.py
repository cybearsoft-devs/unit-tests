"""This module contains unit tests for common package utils functions."""
from typing import Any
from typing import Set
from typing import List
from typing import Type
from typing import Dict
from typing import AnyStr
from typing import Callable
from datetime import datetime

from unittest.mock import Mock
from unittest.mock import patch
from unittest.mock import MagicMock

import jwt
import pytest
from marshmallow import Schema
from freezegun import freeze_time
from flask.ctx import RequestContext
from botocore.client import BaseClient
from _pytest.monkeypatch import MonkeyPatch
from apiflask import HTTPTokenAuth, HTTPError
from pytest_cases import parametrize_with_cases

import common.bus as bus
import common.jwtoken as jwtoken

from common.jwtoken import secret
from common.jwtoken import encode
from common.jwtoken import decode

from common.auth import decode_token
from common.auth import has_any_role
from common.auth import get_auth_email
from common.auth import get_user_roles
from common.auth import get_auth_user_ref
from common.auth import get_auth_group_refs
from common.auth import get_auth_professional_ref

from common.blueprint import PatchProcessor
from common.blueprint import _updates_from_patch

from common.bus import client, dispatch, create, update, delete
from common.patch import PatchProcessor as OriginPatchProcessor

import tests.common.test_cases as cases


@parametrize_with_cases(
    'token, expected_result',
    cases=cases.DecodeTokenPositiveCases
)
def test_decode_token_positive(token: AnyStr, expected_result: List):
    """
    Given token, asserts that:
        - token contains needed data.
    """
    result = decode_token(token).values()

    for item, exp_item in zip(result, expected_result):
        assert item == exp_item


@parametrize_with_cases(
    'token, expected_result',
    cases=cases.DecodeTokenNegativeCases
)
def test_decode_token_negative(token: AnyStr, expected_result: List):
    """
    Given token, asserts that:
        - appropriate exception was raised.
    """
    with pytest.raises(HTTPError) as exc:
        decode_token(token)

    exp_status_code, exp_message = expected_result

    assert exc.value.status_code == exp_status_code
    assert exc.value.message     == exp_message


@parametrize_with_cases(
    'current_user, user, expected_result',
    cases=cases.GetUserRolesCases
)
def test_get_user_roles(monkeypatch: MonkeyPatch, request_context: RequestContext,
                        current_user: Dict, user: Dict, expected_result: List):
    """
    Given user, asserts that:
        - appropriate user roles were returned.
    """
    with request_context:
        monkeypatch.setattr(HTTPTokenAuth, "current_user", current_user)
        result = get_user_roles(user)

        assert result == expected_result


@parametrize_with_cases(
    'user_roles, roles, expected_result',
    cases=cases.HasAnyRoleCases
)
@patch('common.auth.get_user_roles')
def test_has_any_role(get_user_roles: Type[MagicMock], request_context: Type[RequestContext],
                      user_roles: List, roles: List,
                      expected_result: bool):
    """
    Given user, asserts that:
        - user has any of specified role.
    """

    with request_context:
        get_user_roles.return_value = user_roles

        assert has_any_role(roles) == expected_result


@parametrize_with_cases(
    'current_user, expected_result',
    cases=cases.GetAuthUserRefCases
)
def test_get_auth_user_ref(monkeypatch: Type[MonkeyPatch], request_context: Type[RequestContext],
                           current_user: Dict, expected_result: AnyStr):
    """
    Given authenticated user, asserts that:
        - auth user ref equals to '/users/<user.uid>'.
    """

    with request_context:
        monkeypatch.setattr(HTTPTokenAuth, "current_user", current_user)

        assert get_auth_user_ref() == expected_result


@parametrize_with_cases(
    'current_user, expected_result',
    cases=cases.GetAuthGroupRefsCases
)
def test_get_auth_group_refs(monkeypatch: Type[MonkeyPatch], request_context: Type[RequestContext],
                             current_user: Dict, expected_result: AnyStr):
    """
    Given authenticated user, asserts that:
        - auth user' groups ref equals to '/groups/<user.grpids[group_id]>'.
    """

    with request_context:
        monkeypatch.setattr(HTTPTokenAuth, "current_user", current_user)

        assert get_auth_group_refs() == expected_result


@parametrize_with_cases(
    'current_user, expected_result',
    cases=cases.GetAuthEmailCases
)
def test_get_auth_email(monkeypatch: Type[MonkeyPatch], request_context: Type[RequestContext],
                        current_user: Dict, expected_result: AnyStr):
    """
    Given authenticated user, asserts that:
        - correct auth user' email was returned.
    """

    with request_context:
        monkeypatch.setattr(HTTPTokenAuth, "current_user", current_user)

        assert get_auth_email() == expected_result


@parametrize_with_cases(
    'current_user, expected_result',
    cases=cases.GetAuthProfessionalRefCases
)
def test_get_auth_professional_ref(monkeypatch: Type[MonkeyPatch], request_context: Type[RequestContext],
                                   current_user: Dict, expected_result: AnyStr):
    """
    Given authenticated user, asserts that:
        - correct auth user' professional id was returned.
    """

    with request_context:
        monkeypatch.setattr(HTTPTokenAuth, "current_user", current_user)

        assert get_auth_professional_ref() == expected_result


@parametrize_with_cases(
    'test_input, callback, expected_result',
    cases=cases.PatchProcessorAddCases
)
def test_patch_processor_add(test_input: AnyStr, callback: Callable,
                             expected_result: Dict):
    """
    Given path, asserts that:
        - 'add' action was added to PatchProcessor' _table property.
    """
    processor = PatchProcessor()
    callback  = processor.add(callback)

    assert processor._table == dict()

    callback(test_input)

    assert processor._table == expected_result


@parametrize_with_cases(
    'test_input, callback, expected_result',
    cases=cases.PatchProcessorReplaceCases
)
def test_patch_processor_replace(test_input: AnyStr, callback: Callable,
                                 expected_result: Dict):
    """
    Given path, asserts that:
        - 'replace' action was added to PatchProcessor' _table property.
    """
    processor = PatchProcessor()
    callback  = processor.replace(callback)

    assert processor._table == dict()

    callback(test_input)

    assert processor._table == expected_result


@parametrize_with_cases(
    'test_input, callback, expected_result',
    cases=cases.PatchProcessorDeleteCases
)
def test_patch_processor_delete(test_input: AnyStr, callback: Callable,
                                expected_result: Dict):
    """
    Given path, asserts that:
        - 'delete' action was added to PatchProcessor' _table property.
    """
    processor = PatchProcessor()
    callback  = processor.delete(callback)

    assert processor._table == dict()

    callback(test_input)

    assert processor._table == expected_result


@parametrize_with_cases(
    'data, operation, expected_result',
    cases=cases.PatchProcessorCallNegativeCases
)
def test_patch_processor_call_negative(data: AnyStr, operation: Type[Mock],
                                       expected_result: List):
    """
    Given path, asserts that:
        - appropriate exception was raised.
    """
    processor = PatchProcessor()

    with pytest.raises(HTTPError) as exc:
        processor.__call__(data, operation)

    exp_status_code, exp_message = expected_result

    assert exc.value.status_code == exp_status_code
    assert exc.value.message     == exp_message


@parametrize_with_cases(
    'data, operation, test_input, callback, expected_result',
    cases=cases.PatchProcessorCallPositiveCases
)
def test_patch_processor_call_positive(data: AnyStr, operation: Type[Mock],
                                       test_input: AnyStr, callback: Callable,
                                       expected_result: Dict):
    """
    Given path, asserts that:
        - empty list was returned
        in case of PatchProcessor._table item equals to None;
        - item was returned
        in case of PatchProcessor._table item type equals to list;
        - items list was returned
        in case of PatchProcessor._table item type not equals to list;
    """
    processor = PatchProcessor()
    callback  = processor.add(callback)

    assert processor._table == dict()

    callback(test_input)

    assert processor.__call__(data, operation) == expected_result


@parametrize_with_cases(
    'data, _patch, logger, patch_processor, expected_result',
    cases=cases.UpdatesFromPatchNegativeCases
)
def test_updates_from_patch_negative(data: Dict, _patch: List,
                                     logger: Type[Logger], patch_processor: Callable,
                                     expected_result: List):
    """
    Given data, patch, logger and patch_processor, asserts that:
        - appropriate exception was raised.
    """

    with pytest.raises(HTTPError) as exc:
        _updates_from_patch(data, _patch, logger, patch_processor)

    exp_status_code, exp_message, exp_detail = expected_result

    assert exc.value.message     == exp_message
    assert exc.value.status_code == exp_status_code
    assert exc.value.detail      == exp_detail


@parametrize_with_cases(
    'global_client, _client, expected_result',
    cases=cases.ClientCases
)
@patch('common.bus.boto3.client')
def test_client(boto3_client: MagicMock, monkeypatch: Type[MonkeyPatch],
                global_client: Type[BaseClient], _client: Type[BaseClient],
                expected_result: Type[BaseClient]):
    """
    Given _client global variable, asserts that:
        - _client global variable successfully set to boto3 'events' client.
    """
    monkeypatch.setattr(bus, '_client',  global_client)

    boto3_client.return_value = _client

    assert client() == expected_result


@parametrize_with_cases(
    'action, schema, data, '
    'global_client, env_vars, '
    'headers, expected_result',
    cases=cases.DispatchCases
)
def test_dispatch(monkeypatch: Type[MonkeyPatch], request_context: Type[RequestContext],
                  action: AnyStr,
                  schema: Type[Schema], data: Dict[str, Any],
                  global_client: Type[Mock], env_vars: Set,
                  headers: Dict, expected_result: Dict):
    """
    Given action, schema and data, asserts that:
        - correct event was created.
    """

    with request_context:
        for name, value in env_vars:
            monkeypatch.setenv(name, value)

        monkeypatch.setattr(bus, '_client', global_client)
        monkeypatch.setattr(request_context.request, 'headers', headers)

        dispatch(action, schema, data)

        result          = global_client.Entries[0].items()
        expected_result = expected_result.items()

        for (key, value), (exp_key, exp_value) in zip(result, expected_result):
            assert key == exp_key
            if isinstance(value, datetime):
                assert value.date() == exp_value.date()
            else:
                assert value == exp_value


@parametrize_with_cases(
    'schema, data, expected_result',
    cases=cases.CreateCases
)
@patch('common.bus.dispatch')
def test_create(dispatch_: Type[MagicMock], schema: Type[Schema],
                data: Dict[str, Any], expected_result: Dict):
    """
    Given schema and data, asserts that:
        - dispatch was called for 'create' action.
    """
    dispatch_.side_effect = lambda *args: args

    assert create(schema, data) == (expected_result, schema, data)


@parametrize_with_cases(
    'schema, data, expected_result',
    cases=cases.UpdateCases
)
@patch('common.bus.dispatch')
def test_update(dispatch_: Type[MagicMock], schema: Type[Schema],
                data: Dict[str, Any], expected_result: Dict):
    """
    Given schema and data, asserts that:
        - dispatch was called for 'update' action.
    """
    dispatch_.side_effect = lambda *args: args

    assert update(schema, data) == (expected_result, schema, data)


@parametrize_with_cases(
    'schema, data, expected_result',
    cases=cases.DeleteCases
)
@patch('common.bus.dispatch')
def test_delete(dispatch_: Type[MagicMock], schema: Type[Schema],
                data: Dict[str, Any], expected_result: Dict):
    """
    Given schema and data, asserts that:
        - dispatch was called for 'delete' action.
    """
    dispatch_.side_effect = lambda *args: args

    assert delete(schema, data) == (expected_result, schema, data)


@parametrize_with_cases(
    'global_secret, _secret, expected_result',
    cases=cases.SecretCases
)
@patch('common.jwtoken.boto3.client')
def test_secret(boto3_client: MagicMock, monkeypatch: Type[MonkeyPatch],
                global_secret: AnyStr, _secret: AnyStr,
                expected_result: AnyStr):
    """
    Given _secret global variable, asserts that:
        - value for _secret global variable was successfully extracted
        from boto3 'secretsmanager' client.
    """
    boto3_client.return_value = _secret

    monkeypatch.setattr(jwtoken, '_secret', global_secret)
    monkeypatch.setenv('JWT_SECRET', 'test_jwt')

    assert secret() == expected_result


@parametrize_with_cases(
    'payload, expiry, secret_string, '
    'env_vars, dt, expected_result',
    cases=cases.EncodeCases
)
@patch('common.jwtoken.secret')
def test_encode(secret: MagicMock, monkeypatch: Type[MonkeyPatch],
                payload: Dict, expiry: int, secret_string: AnyStr,
                env_vars: List, dt: datetime,
                expected_result: List):
    """
    Given payload and expiry, asserts that:
        - jwt was successfully encoded.
    """
    with freeze_time(dt):
        secret.return_value = secret_string
        for name, value in env_vars:
            monkeypatch.setenv(name, value)

        result       = encode(payload, expiry)
        decoded_data = jwt.decode(result, secret_string, algorithms=['HS256'])

        exp_iss, exp_exp = expected_result

        assert decoded_data.get('iss') == exp_iss
        assert decoded_data.get('exp') == exp_exp


@parametrize_with_cases(
    'token, audience, env_vars, '
    'secret_string, expected_result',
    cases=cases.DecodeCases
)
@patch('common.jwtoken.secret')
def test_decode(secret: MagicMock, monkeypatch: Type[MonkeyPatch],
                token: AnyStr, audience: AnyStr,
                env_vars: List, secret_string: AnyStr,
                expected_result: List):
    """
    Given payload and expiry, asserts that:
        - jwt was successfully decoded.
    """
    secret.return_value = secret_string

    for name, value in env_vars:
        monkeypatch.setenv(name, value)

    result = decode(token, audience)

    exp_iss, exp_aud = expected_result

    assert result.get('iss') == exp_iss
    assert result.get('aud') == exp_aud


@parametrize_with_cases(
    'path, expected_result',
    cases=cases.OriginProcessorAddCases
)
def test_origin_processor_add(monkeypatch: Type[MonkeyPatch], path: Callable,
                              expected_result: List):
    """
    Given path, asserts that:
        - PatchProcessor' 'route' method called
        with 'ADD' method.
    """
    monkeypatch.setattr(OriginPatchProcessor, 'route', lambda *args: args)

    patch_processor = OriginPatchProcessor()

    exp_path, exp_method = expected_result
    _, path_, method_    = patch_processor.add(path)

    assert path_   == exp_path
    assert method_ == exp_method


@parametrize_with_cases(
    'path, expected_result',
    cases=cases.OriginProcessorReplaceCases
)
def test_origin_processor_replace(monkeypatch: Type[MonkeyPatch], path: Callable,
                                  expected_result: List):
    """
    Given path, asserts that:
        - PatchProcessor' 'route' method called
        with 'REPLACE' method.
    """
    monkeypatch.setattr(OriginPatchProcessor, 'route', lambda *args: args)

    patch_processor = OriginPatchProcessor()

    exp_path, exp_method = expected_result
    _, path_, method_    = patch_processor.replace(path)

    assert path_   == exp_path
    assert method_ == exp_method


@parametrize_with_cases(
    'path, expected_result',
    cases=cases.OriginProcessorDeleteCases
)
def test_origin_processor_delete(monkeypatch: Type[MonkeyPatch], path: Callable,
                                 expected_result: List):
    """
    Given path, asserts that:
        - PatchProcessor' 'route' method called
        with 'DELETE' method.
    """
    monkeypatch.setattr(OriginPatchProcessor, 'route', lambda *args: args)

    patch_processor = OriginPatchProcessor()

    exp_path, exp_method = expected_result
    _, path_, method_    = patch_processor.delete(path)

    assert path_   == exp_path
    assert method_ == exp_method


@parametrize_with_cases(
    'path, method, callback, expected_result',
    cases=cases.OriginProcessorRouteCases
)
def test_origin_processor_route(path: AnyStr, method: AnyStr,
                                callback, expected_result: List):
    """
    Given path and method, asserts that:
        - input function was successfully decorated by route method.
    """
    patch_processor = OriginPatchProcessor()

    route = getattr(patch_processor, method)(path)

    route(callback)

    exp_method, exp_endpoint, exp_rule = expected_result

    rules = patch_processor._map._rules[0]

    assert next(iter(rules.methods)) == exp_method
    assert rules.endpoint            == exp_endpoint
    assert rules.rule                == exp_rule


@parametrize_with_cases(
    'path, method, callback, '
    'data, operation, expected_result',
    cases=cases.OriginProcessorCallCases
)
def test_origin_processor_call(path: AnyStr, method: AnyStr,
                               callback: Callable, data: AnyStr,
                               operation: Type[Mock], expected_result: List):
    """
    Given data and operation, asserts that:
        - empty list was returned
        in case of PatchProcessor.endpoint item equals to None;
        - item was returned
        in case of PatchProcessor.endpoint item type equals to list;
        - items list was returned
        in case of PatchProcessor.endpoint item type not equals to GeneratorType;
        - else items list was returned.
    """
    patch_processor = OriginPatchProcessor()

    route = getattr(patch_processor, method)(path)

    route(callback)

    result = patch_processor.__call__(data, operation)

    assert result == expected_result


@parametrize_with_cases(
    'data, patch_, path, '
    'method, callback, expected_result',
    cases=cases.ProcessorGetUpdatesPositiveCases
)
def test_origin_processor_get_updates_positive(data: AnyStr, patch_: List,
                                               path: AnyStr, method: AnyStr,
                                               callback: Callable, expected_result: List):
    """
    Given data and patch, asserts that:
        - appropriate operations were returned.
    """
    patch_processor = OriginPatchProcessor()

    route = getattr(patch_processor, method)(path)

    route(callback)

    result = patch_processor.get_updates(data, patch_)

    assert result == expected_result


@parametrize_with_cases(
    'data, patch_, path, '
    'method, callback, expected_result',
    cases=cases.ProcessorGetUpdatesNegativeCases
)
def test_origin_processor_get_updates_negative(data: AnyStr, patch_: List,
                                               path: AnyStr, method: AnyStr,
                                               callback: Callable, expected_result: List):
    """
    Given data and patch, asserts that:
        - appropriate exception was raised.
    """
    patch_processor = OriginPatchProcessor()
    route           = getattr(patch_processor, method)(path)

    route(callback)

    with pytest.raises(HTTPError) as exc:
        patch_processor.get_updates(data, patch_)

    exp_status_code, exp_message = expected_result

    assert exc.value.status_code == exp_status_code
    assert exc.value.message     == exp_message
