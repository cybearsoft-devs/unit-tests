"""This module contains unit tests for common db models."""

from typing import List
from typing import Type
from typing import AnyStr
from typing import Callable

from unittest.mock import Mock
from unittest.mock import patch
from unittest.mock import MagicMock

from http.client import HTTPResponse

from _pytest.monkeypatch import MonkeyPatch

from pytest_cases import parametrize_with_cases

from domain.user.schema import User
from domain.user.model import UserModel

from common.model import boto3
from common.model import parse_ref
from common.model import ETagMixin
from common.model import store_picture
from common.model import store_pictures
from common.model import modify_from_data

from tests.common.test_cases import ETagMixin as ETM

import tests.common.test_cases as cases


@parametrize_with_cases(
    'ref, expected_result',
    cases=cases.ParseRefCases
)
def test_parse_ref(ref: AnyStr, expected_result: List):
    """
    Given ref, asserts that:
        - ref, id and title was successfully parsed from ref.
    """

    result = parse_ref(ref)

    exp_id, exp_ref, exp_title = expected_result

    assert result.id    == exp_id
    assert result.ref   == exp_ref
    assert result.title == exp_title


@parametrize_with_cases(
    'base, data, expected_result',
    cases=cases.ModifyFromDataCases
)
def test_modify_from_data(base: Type[UserModel], data: Type[User],
                          expected_result: Type[UserModel]):
    """
    Given base and data, asserts that:
        - that base model was successfully updated
        with provided data.
    """

    result = modify_from_data(base, data)

    assert result.email == expected_result


@parametrize_with_cases(
    'response, static_prefix, static_url, '
    'static_bucket, tags, s3, value, '
    'ref, expected_result',
    cases=cases.StorePictureCases
)
@patch('common.model.urlopen')
def test_store_picture(urlopen: MagicMock, monkeypatch: Type[MonkeyPatch],
                       response: HTTPResponse, static_prefix: AnyStr,
                       static_url: AnyStr, static_bucket: AnyStr,
                       tags: AnyStr, s3: Type[Mock],
                       value: AnyStr, ref: AnyStr,
                       expected_result: AnyStr):
    """
    Given base and data, asserts that:
        - ref to picture stored on s3 was successfully returned.
    """

    urlopen.return_value.__enter__.return_value = response

    monkeypatch.setattr(boto3, 'resource', s3)

    monkeypatch.setenv('TAGS',          tags)
    monkeypatch.setenv('STATIC_URL',    static_url)
    monkeypatch.setenv('STATIC_PREFIX', static_prefix)
    monkeypatch.setenv('STATIC_BUCKET', static_bucket)

    assert store_picture(value, ref) == expected_result


@parametrize_with_cases(
    'picture_ref, base, data, '
    'ref, expected_result',
    cases=cases.StorePicturesCases
)
@patch('common.model.store_picture')
def test_store_pictures(store_picture: MagicMock, picture_ref: Callable,
                        base: Type[UserModel], data: Type[User],
                        ref: AnyStr, expected_result: List):
    """
    Given base and data, asserts that:
        - ref to picture was successfully assigned
        as value for base model 'picture' field.
    """

    store_picture.return_value = picture_ref

    assert base.picture is None

    store_pictures(base, data, ref)

    assert base.picture == expected_result


@parametrize_with_cases(
    'e_tag_mixin, expected_result',
    cases=cases.ETagMixinRefHashCases
)
def test_e_tag_mixin_ref_hash(e_tag_mixin: ETagMixin, expected_result: AnyStr):
    """
    Given ETagMixin instance, asserts that:
        - appropriate ref_hash property was successfully returned.
    """

    assert e_tag_mixin._ref_hash == expected_result


@parametrize_with_cases(
    'ref_hash, e_tag_mixin, expected_result',
    cases=cases.ETagMixinGetEtagCases
)
def test_e_tag_mixin_get_etag(monkeypatch: Type[MonkeyPatch], ref_hash: AnyStr,
                              e_tag_mixin: ETagMixin, expected_result: AnyStr):
    """
    Given ETagMixin object, asserts that:
        - appropriate etag property was successfully returned.
    """
    monkeypatch.setattr(ETM, '_ref_hash', ref_hash)

    assert e_tag_mixin.etag == expected_result


@parametrize_with_cases(
    'ref_hash, etag, e_tag_mixin, expected_result',
    cases=cases.ETagMixinSetEtagCases
)
def test_e_tag_mixin_set_etag(monkeypatch: Type[MonkeyPatch], ref_hash: AnyStr,
                              etag: AnyStr, e_tag_mixin: ETagMixin,
                              expected_result: List):
    """
    Given ETagMixin object, asserts that:
        - appropriate etag property was successfully set.
    """

    monkeypatch.setattr(ETM, '_ref_hash', ref_hash)

    e_tag_mixin.etag = etag

    etag_, version = expected_result

    assert e_tag_mixin.etag    == etag_
    assert e_tag_mixin.version == version
