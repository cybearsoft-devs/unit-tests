"""This module contains unit tests cases."""
import json
import logging

from typing import Tuple, List
from datetime import datetime
from unittest.mock import Mock

import jwt
import boto3

from shortuuid import uuid
from flask import Response
from werkzeug.exceptions import HTTPException

from common.common.model import ETagMixin

from tests.utils import user, user_data, movie, movie_data


user_id  = uuid()
movie_id = uuid()

UNAUTHORIZED_EXC      = 'Unauthorized'
INVALID_OPERATION_EXC = 'Invalid operation {} for {}'
PATCH_ERR_EXC         = 'Patch.error'


class DecodeTokenPositiveCases:
    aud        = 'test_aud'
    decoded_id = '{}@{}'.format(user_id, aud)

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - valid token;
            - data which matches token' data
            as expected result.
        """
        data = dict(id=uuid(), user_id=user_id, aud=self.aud)

        valid_token     = jwt.encode(data, 'test_secret', algorithm='HS256')
        expected_result = [self.decoded_id, user_id, self.aud]

        return valid_token, expected_result


class DecodeTokenNegativeCases:
    invalid_token = 'invalid_token'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - invalid token;
            - Unauthorized error as expected result;
        """
        expected_result = [401, UNAUTHORIZED_EXC]

        return self.invalid_token, expected_result


class GetUserRolesCases:
    scope = 'test_scope'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - auth user equals None;
            - user with scopes;
            - scopes which are common
            with user' scopes as expected result.
        """
        auth_user       = None
        user            = dict(id=user_id, scope=self.scope)
        expected_result = [self.scope]

        return auth_user, user, expected_result

    def case_2(self) -> Tuple:
        """
        Inputs are:
            - auth user with scopes;
            - user equals None;
            - scopes which are common
            with user' scopes as expected result.
        """
        auth_user       = dict(id=user_id, scope=self.scope)
        user            = None
        expected_result = [self.scope]

        return auth_user, user, expected_result


class HasAnyRoleCases:
    scope = 'test_scope'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - user with scopes;
            - scopes which are common with user' scopes;
            - positive expected result.
        """
        user_scopes     = [self.scope]
        scopes          = [self.scope]
        expected_result = True

        return user_scopes, scopes, expected_result

    def case_2(self) -> Tuple:
        """
        Inputs are:
            - empty user' scopes;
            - scopes which are not common with user' scopes;
            - negative expected result.
        """
        user_scopes     = []
        scopes          = [self.scope]
        expected_result = False

        return user_scopes, scopes, expected_result


class GetAuthUserRefCases:
    user_ref = '/users/{}'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - authenticated user;
            - authenticated user ref equals to
            '/users/<user.uid>' as expected result.
        """
        auth_user       = dict(uid=user_id)
        expected_result = self.user_ref.format(user_id)

        return auth_user, expected_result


class GetAuthGroupRefsCases:
    group_1 = uuid()
    group_2 = uuid()
    grpids  = '{},{}'.format(group_1, group_2)

    groups_ref = '/groups/{}'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - authenticated user;
            - authenticated user' groups ref equals to
            '/groups/<user.grpids.[group_id]>' as expected result.
        """
        auth_user       = dict(uid=user_id, grpids=self.grpids)
        expected_result = [
            self.groups_ref.format(self.group_1),
            self.groups_ref.format(self.group_2)
        ]

        return auth_user, expected_result


class GetAuthEmailCases:
    email = 'test@email.com'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - authenticated user;
            - authenticated user' email as expected result.
        """
        auth_user       = dict(uid=user_id, email=self.email)
        expected_result = self.email

        return auth_user, expected_result


class GetAuthProfessionalRefCases:
    professional_id = uuid()

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - authenticated user;
            - authenticated user' professional id as expected result.
        """
        auth_user       = dict(uid=user_id, proid=self.professional_id)
        expected_result = self.professional_id

        return auth_user, expected_result


class PatchProcessorAddCases:
    test_input = 'test'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - test input;
            - test function;
            - list containing test function
            with appropriate action type as expected result.
        """
        test_input      = self.test_input
        test_function   = self._test_func
        expected_result = {('add', self._test_func): self.test_input}

        return test_input, test_function, expected_result

    @staticmethod
    def _test_func(data):
        return data


class PatchProcessorReplaceCases:
    test_input = 'test'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - test input;
            - test function;
            - list containing test function
            with appropriate action type as expected result.
        """
        test_input      = self.test_input
        test_function   = self._test_func
        expected_result = {('replace', self._test_func): self.test_input}

        return test_input, test_function, expected_result

    @staticmethod
    def _test_func(data):
        return data


class PatchProcessorDeleteCases:
    test_input = 'test'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - test input;
            - test function;
            - list containing test function
            with appropriate action type as expected result.
        """
        test_input = self.test_input
        test_function = self._test_func
        expected_result = {('delete', self._test_func): self.test_input}

        return test_input, test_function, expected_result

    @staticmethod
    def _test_func(data):
        return data


class PatchProcessorCallNegativeCases:
    data             = ''
    not_exs_op   = 'not_existed_op'
    not_exs_path = 'not_existed_path'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - not existed data;
            - not existed operation;
            - invalid operation error as expected result.
        """
        data             = self.data
        operation        = Mock(op=self.not_exs_op, path=self.not_exs_path)
        expected_result  = \
            [400, INVALID_OPERATION_EXC.format(self.not_exs_op, self.not_exs_path)]

        return data, operation, expected_result


class PatchProcessorCallPositiveCases:
    op    = 'add'
    data  = 'test_data'
    value = 'test_value'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - existed data;
            - existed operation;
            - test input equals None;
            - test function;
            - empty list as expected result.
        """
        data            = self.data
        test_input      = self._none_test_input
        test_function   = self._test_func
        operation       = Mock(op=self.op, path=test_function, value=self.value)
        expected_result = []

        return data, operation, test_input, test_function, expected_result

    def case_2(self) -> Tuple:
        """
        Inputs are:
            - existed data;
            - existed operation;
            - test input with type of list;
            - test function;
            - list containing data and
            value as expected result.
        """
        data            = self.data
        test_input      = self._list_test_input
        test_function   = self._test_func
        operation       = Mock(op=self.op, path=test_function, value=self.value)
        expected_result = [data, self.value]

        return data, operation, test_input, test_function, expected_result

    def case_3(self) -> Tuple:
        """
        Inputs are:
            - existed data;
            - existed operation;
            - test input with type of tuple;
            - test function;
            - list containing data and
            value as expected result.
        """
        data            = self.data
        test_input      = self._not_list_test_input
        test_function   = self._test_func
        operation       = Mock(op=self.op, path=test_function, value=self.value)
        expected_result = [(data, self.value)]

        return data, operation, test_input, test_function, expected_result

    @staticmethod
    def _none_test_input(data, value) -> None:
        return None

    @staticmethod
    def _list_test_input(data, value) -> List:
        return [data, value]

    @staticmethod
    def _not_list_test_input(data, value) -> Tuple:
        return data, value

    @staticmethod
    def _test_func(data):
        return data


class UpdatesFromPatchNegativeCases:
    op    = 'add'
    data  = 'test_data'
    value = 'test_value'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - data;
            - patch list;
            - logger;
            - function raising HTTPException;
            - patch error as expected result.
        """
        data      = self.data
        operation = Mock(op=self.op, path=self._test_func, value=self.value)
        _patch    = [operation]
        logger    = logging.getLogger(__name__)
        error     = dict(op=operation, error='??? Unknown Error: None')

        patch_processor = self._exc_test_func
        expected_result = [400, PATCH_ERR_EXC, dict(errors=[error])]

        return data, _patch, logger, patch_processor, expected_result

    @staticmethod
    def _test_func(data):
        return data

    @staticmethod
    def _exc_test_func(data, operation):
        raise HTTPException()


class ClientCases:
    client = 'events'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - global _client variable equals None;
            - boto3 client;
            - global _client variable equals new boto3 client as expected result.
        """
        global_client   = None
        _client         = boto3.client(self.client)
        expected_result = _client

        return global_client, _client, expected_result

    def case_2(self) -> Tuple:
        """
        Inputs are:
            - global _client variable equals boto3 client;
            - boto3 client;
            - global _client variable equals old boto3 client as expected result.
        """
        global_client   = boto3.client(self.client)
        _client         = None
        expected_result = global_client

        return global_client, _client, expected_result


class DispatchCases:
    ref          = 'test_ref'
    tag          = 'test_tag'
    source       = 'upfeel.backend'
    event_bus    = 'test_dispatch'
    action_type  = 'create'
    schema_title = 'test_schema'
    trace_header = 'test_header'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - action type;
            - data schema;
            - data;
            - global _client variable equals boto3 client;
            - EVENT_BUS environment variables;
            - 'x-amzn-trace-id' header;
            - None as expected result.
        """
        data            = self.data
        schema          = self.schema
        action_type     = self.action_type
        headers         = {'x-amzn-trace-id': self.trace_header}
        global_client   = self.global_client
        env_vars        = [('EVENT_BUS', self.event_bus)]
        message         = dict(data=dict(), schema=self.schema_title, ref=self.ref,
                               action=self.action_type)
        expected_result = dict(Time=datetime.now(), Source=self.source, EventBusName=self.event_bus,
                               DetailType=self._detail_type, Detail=json.dumps(message),
                               TraceHeader=self.trace_header)

        return action_type, schema, data, global_client, env_vars, headers, expected_result

    def case_2(self) -> Tuple:
        """
        Inputs are:
            - action type;
            - data schema;
            - data;
            - global _client variable equals boto3 client;
            - EVENT_BUS and TAGS environment variables;
            - empty headers;
            - None as expected result.
        """
        data            = self.data
        schema          = self.schema
        action_type     = self.action_type
        headers         = dict()
        global_client   = self.global_client
        env_vars        = [('EVENT_BUS', self.event_bus), ('TAGS', json.dumps(self._tags))]
        message         = dict(data=dict(), schema=self.schema_title, ref=self.ref,
                               action=self.action_type, context=self._tags)
        expected_result = dict(Time=datetime.now(), Source=self.source, EventBusName=self.event_bus,
                               DetailType=self._detail_type, Detail=json.dumps(message),
                               TraceHeader='none')

        return action_type, schema, data, global_client, env_vars, headers, expected_result

    @property
    def schema(self):
        return Mock(return_value=self._schema_value, Meta=self._schema_meta)

    @property
    def data(self):
        return Mock(ref=self.ref)

    @property
    def global_client(self):
        def _set_entries(Entries):
            setattr(global_client, 'Entries', Entries)

        global_client = Mock(Entries=[], put_events=lambda Entries: _set_entries(Entries))

        return global_client

    @property
    def _tags(self):
        return dict(name=self.tag)

    @property
    def _schema_value(self):
        return Mock(dump=lambda args: dict())

    @property
    def _detail_type(self):
        return self.schema_title + self.action_type.capitalize()

    @property
    def _schema_meta(self):
        return Mock(title=Mock(title=lambda: self.schema_title))


class CreateCases:
    action = 'create'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - schema;
            - data;
            - 'create' action as expected result.
        """

        return 'schema', 'data', self.action


class UpdateCases:
    action = 'update'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - schema;
            - data;
            - 'update' action as expected result.
        """

        return 'schema', 'data', self.action


class DeleteCases:
    action = 'delete'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - schema;
            - data;
            - 'delete' action as expected result.
        """

        return 'schema', 'data', self.action


class SecretCases:
    secret = 'test_secret'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - global _secret variable equals None;
            - jwt secret;
            - jwt secret equals SecretString from secretsmanager
            as expected result.
        """
        global_secret = None
        _secret = Mock(get_secret_value=lambda SecretId: self._secret_string)
        expected_result = self.secret

        return global_secret, _secret, expected_result

    def case_2(self) -> Tuple:
        """
        Inputs are:
            - global _secret variable equals None;
            - jwt secret;
            - jwt secret equals global _secret variable
            as expected result.
        """
        global_secret   = self.secret
        _secret         = None
        expected_result = global_secret

        return global_secret, _secret, expected_result

    @property
    def _secret_string(self):
        return dict(SecretString=self.secret)


class EncodeCases:
    dt             = '1970-01-01 12:00:01'
    secret_string  = 'test_secret'
    stack          = 'test_stack'
    default_expiry = 60

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - jwt payload;
            - expiry equals 60 seconds;
            - secret_string;
            - TAGS environment variable;
            - static date and time;
            - encoded jwt with 'iss' and 'exp' keys as expected result.
        """
        payload         = dict()
        expiry          = self.default_expiry
        secret_string   = self.secret_string
        env_vars        = [('TAGS', json.dumps(dict(Stack=self.stack)))]
        dt              = self.dt
        expected_result = [self.stack, 43261]

        return payload, expiry, secret_string, env_vars, dt, expected_result

    def case_2(self) -> Tuple:
        """
        Inputs are:
            - jwt payload;
            - expiry equals None;
            - secret_string;
            - empty environment variables;
            - static date and time;
            - encoded jwt with no 'iss' and 'exp' keys as expected result.
        """
        payload         = dict()
        expiry          = None
        secret_string   = self.secret_string
        env_vars        = []
        dt              = self.dt
        expected_result = [None, None]

        return payload, expiry, secret_string, env_vars, dt, expected_result


class DecodeCases:
    aud           = 'test_aud'
    stack         = 'test_stack'
    secret_string = 'test_secret'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - encoded token with 'iss' and 'aud' keys;
            - audience;
            - TAGS environment variable;
            - 'iss' and 'aud' keys as expected result.
        """
        token           = ('qwerty')
        aud             = self.aud
        env_vars        = [('TAGS', json.dumps(dict(Stack=self.stack)))]
        secret_string   = self.secret_string
        expected_result = [self.stack, aud]

        return token, aud, env_vars, secret_string, expected_result

    def case_2(self) -> Tuple:
        """
        Inputs are:
            - encoded token with empty payload;
            - audience;
            - empty environment variables;
            - 'iss' and 'aud' keys equals None as expected result.
        """
        token           = ('qwerty')
        aud             = None
        env_vars        = []
        secret_string   = self.secret_string
        expected_result = [None, aud]

        return token, aud, env_vars, secret_string, expected_result


class ParseRefCases:
    title  = 'test_parse_ref'
    id_    = 'test'
    params = '?#title={}'.format(title)
    path   = '/3/library/{}'.format(id_)
    ref    = 'http://www.example.com{}{}'.format(path, params)

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - test ref equals '<scheme>://<netloc>/<path>;<params>?<query>#<fragment>' pattern;
            - namedtuple with ref, id and title as expected result.
        """
        ref             = self.ref
        expected_result = [self.id_, self.path, [self.title]]

        return ref, expected_result


class ModifyFromDataCases:
    email     = 'test@email.com'
    new_email = 'new_test@email.com'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - base user model;
            - email as data to be updated;
            - new email as expected result.
        """
        base            = user(dict(id=user_id, email=self.email))
        data            = user_data(dict(email=self.new_email))
        expected_result = self.new_email

        return base, data, expected_result


class StorePictureCases:
    data  = 'test_data'
    stack = 'test_stack'
    value = 'test_value'
    ref   = 'test_ref'

    STATIC_URL    = 'test_static_url'
    STATIC_PREFIX = 'test_static_prefix'
    STATIC_BUCKET = 'test_static_bucket'

    headers = {
        'Content-Type': 'text/html; charset=utf-8'
    }

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - response as a result of urlopen call;
            - STATIC_PREFIX environment variable;
            - STATIC_URL environment variable;
            - TAGS environment variable;
            - fake s3 boto3 client;
            - test value;
            - test ref;
            - ref to stored picture as expected result.
        """
        response = Mock(read=lambda: self.data.encode('utf-8'), headers=self.headers)
        prefix   = self.STATIC_PREFIX
        url      = self.STATIC_URL
        bucket   = self.STATIC_BUCKET
        tags     = json.dumps(dict(Stack=self.stack))
        s3       = Mock(Bucket=lambda: Mock(put_object=lambda: None))
        value    = self.value
        ref      = self.ref

        expected_result = 'test_static_url/test_static_prefixtest_ref/6af8307cNone'

        return response, prefix, url, bucket, tags, s3, value, ref, expected_result


class StorePicturesCases:
    picture_ref = 'data:test_picture_ref'
    email       = 'test_email'
    new_email   = 'new_test_email'
    ref         = 'test_ref'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - test ref to stored picture;
            - base user model;
            - email as data to be updated;
            - test ref;
            - ref to stored picture as expected result.
        """
        picture_ref = self.picture_ref
        base        = movie(dict(id=movie_id))
        data        = movie_data(dict(picture=self.picture_ref))
        ref         = self.ref

        expected_result = picture_ref

        return picture_ref, base, data, ref, expected_result


class ETagMixinRefHashCases:
    hash_keyname  = 'test_hash_keyname'
    range_keyname = 'test_range_keyname'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - ETagMixin instance;
            - correct ref_hash as expected result.
        """
        e_tag_mixin = ETagMixin()
        e_tag_mixin._hash_keyname = self.hash_keyname
        e_tag_mixin._range_keyname = self.range_keyname

        expected_result = '675a3b9730c8c488253375656824a122'

        return e_tag_mixin, expected_result


class ETagMixinGetEtagCases:
    ref_hash = 'test_ref_hash'
    version  = 1

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - test ref_hash;
            - ETagMixin instance;
            - correct etag as expected result.
        """
        ref_hash            = self.ref_hash
        e_tag_mixin         = ETagMixin()
        e_tag_mixin.version = self.version

        expected_result = '"test_ref_hash-1"'

        return ref_hash, e_tag_mixin, expected_result


class ETagMixinSetEtagCases:
    version  = 1
    ref_hash = 'test_ref_hash'
    value    = 'test_value-{}'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - test ref_hash;
            - test value with version equals '1';
            - ETagMixin instance;
            - correct etag and version as expected result.
        """
        ref_hash            = self.ref_hash
        value               = self.value.format(self.version)
        e_tag_mixin         = ETagMixin()
        e_tag_mixin.version = None

        expected_result = ['"test_ref_hash-1"', self.version]

        return ref_hash, value, e_tag_mixin, expected_result

    def case_2(self) -> Tuple:
        """
        Inputs are:
            - test ref_hash;
            - test value with version equals '0';
            - ETagMixin instance;
            - correct etag and version as expected result.
        """
        ref_hash            = self.ref_hash
        value               = self.value.format('0')
        e_tag_mixin         = ETagMixin()
        e_tag_mixin.version = None

        expected_result = ['"test_ref_hash-0"', None]

        return ref_hash, value, e_tag_mixin, expected_result


class OriginProcessorAddCases:
    path   = 'test_path'
    method = 'ADD'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - test path;
            - patch and 'ADD' method as expected result.
        """
        path = self.path

        expected_result = [path, self.method]

        return path, expected_result


class OriginProcessorReplaceCases:
    path   = 'test_path'
    method = 'REPLACE'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - test path;
            - patch and 'REPLACE' method as expected result.
        """
        path = self.path

        expected_result = [path, self.method]

        return path, expected_result


class OriginProcessorDeleteCases:
    path = 'test_path'
    method = 'DELETE'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - test path;
            - patch and 'DELETE' method as expected result.
        """
        path = self.path

        expected_result = [path, self.method]

        return path, expected_result


class OriginProcessorRouteCases:
    path   = '/test_path'
    method = 'ADD'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - test path;
            - test callback;
            - list containing method, endpoint and path as expected result.
        """
        path     = self.path
        method   = self.method.lower()
        callback = self._test_func

        expected_result = [self.method, callback.__name__, path]

        return path, method, callback, expected_result

    @staticmethod
    def _test_func():
        return


class OriginProcessorCallCases:
    data   = 'test_data'
    method = 'ADD'
    path   = '/test_path'
    value  = 'test_value'

    def case_1(self) -> Tuple:
        """
        Inputs are:
            - test path;
            - test method;
            - test callback which returns None;
            - test data;
            - operation;
            - empty list as expected result.
        """
        path      = self.path
        method    = self.method.lower()
        callback  = self._none_test_func
        data      = self.data
        operation = Mock(op=self.method.lower(), path=self.path, value=self.value)

        expected_result = []

        return path, method, callback, data, operation, expected_result

    def case_2(self) -> Tuple:
        """
        Inputs are:
            - test path;
            - test method;
            - test callback which returns list;
            - test data;
            - operation;
            - test callback return value as expected result.
        """
        path      = self.path
        method    = self.method.lower()
        callback  = self._list_test_func
        data      = self.data
        operation = Mock(op=self.method.lower(), path=self.path, value=self.value)

        expected_result = callback()

        return path, method, callback, data, operation, expected_result

    def case_3(self) -> Tuple:
        """
        Inputs are:
            - test path;
            - test method;
            - test callback which returns generator;
            - test data;
            - operation;
            - list of test callback return value as expected result.
        """
        path      = self.path
        method    = self.method.lower()
        callback  = self._generator_test_func
        data      = self.data
        operation = Mock(op=self.method.lower(), path=self.path, value=self.value)

        expected_result = list(callback())

        return path, method, callback, data, operation, expected_result

    def case_4(self) -> Tuple:
        """
        Inputs are:
            - test path;
            - test method;
            - test callback which returns empty string;
            - test data;
            - operation;
            - list of test callback return value as expected result.
        """
        path = self.path
        method = self.method.lower()
        callback = self._str_test_func
        data = self.data
        operation = Mock(op=self.method.lower(), path=self.path, value=self.value)

        expected_result = [callback()]

        return path, method, callback, data, operation, expected_result

    @staticmethod
    def _none_test_func(*args):
        return None

    @staticmethod
    def _list_test_func(*args):
        return []

    @staticmethod
    def _generator_test_func(*args):
        yield []

    @staticmethod
    def _str_test_func(*args):
        return ''


class ProcessorGetUpdatesPositiveCases:
    data   = 'test_data'
    method = 'ADD'
    path   = '/test_path'
    value  = 'test_value'

    def case_1(self):
        """
        Inputs are:
            - test data;
            - list of test operations;
            - test path;
            - test method;
            - test callback which returns empty string;
            - list of found operations as expected result.
        """
        data     = self.data
        patch_   = [Mock(op=self.method.lower(), path=self.path, value=self.value)]
        path     = self.path
        method   = self.method.lower()
        callback = self._str_test_func

        expected_result = [callback()]

        return data, patch_, path, method, callback, expected_result

    @staticmethod
    def _str_test_func(*args):
        return ''


class ProcessorGetUpdatesNegativeCases:
    data   = 'test_data'
    method = 'REPLACE'
    path   = '/test_path'
    value  = 'test_value'
    status = 'test_status'

    def case_1(self):
        """
        Inputs are:
            - test data;
            - list of test operations;
            - test path;
            - test method;
            - test callback which returns empty string;
            - list of found operations as expected result.
        """
        data = self.data
        patch_ = [Mock(op=self.method.lower(), path=self.path, value=self.value)]
        path = self.path
        method = self.method.lower()
        callback = self._response_test_func

        expected_result = [400, PATCH_ERR_EXC]

        return data, patch_, path, method, callback, expected_result

    @staticmethod
    def _response_test_func(*args):
        return Response()
