"""This module contains unit tests configurations and testcases, which are used by test functions."""
import pytest

from domain.event.app import app


@pytest.fixture(scope="module")
def request_context():
    return app.test_request_context()
